package co.simplon.promo16;

import java.net.MalformedURLException;

import co.simplon.promo16.oop.personne;
import co.simplon.promo16.swing.fenetre;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws MalformedURLException
    {
        System.out.println( "Hello World!" );

        personne perso = new personne(16, "Basil");

        fenetre f = new fenetre(500, perso);
        f.initimg();
        f.initfenetre();
    }
}
    