package co.simplon.promo16.oop;

import java.util.Scanner;

public class personne {
    
    private String p_prenom;
    private String p_nom;
    private int p_age;
    private int p_energy;

    public personne(int age, String prenom){
        p_nom = "Smith";
        p_age = age;
        p_prenom = prenom;
        p_energy = 100;
    }

    public personne(int age, String prenom, String nom){
        p_nom = nom;
        p_age = age;
        p_prenom = prenom;
        p_energy = 100;
    }
    
    public String getprenom(){
        return p_prenom;
    }
    public int getage() {
        return p_age;
    }
    public String getnom() {
        return p_nom;
    }
    public int getenergy(){
        return p_energy;
    }

    public void setP_age(int p_age) {
        this.p_age = p_age;
    }

    public void setP_nom(String p_nom) {
        this.p_nom = p_nom;
    }

    public void setprenom(String prenom){
        p_prenom = prenom;
    }
    public void setenergy(int energy){
        p_energy = energy;
    }

    public boolean ismajeur() {
       
        if (p_age>=18){
            return true;
        }
        else return false;
    }

    public void sleep(){
      
        int i = (int)(Math.random() * (40 - 20)+20);
 
        if (p_energy+i>=100){
            p_energy = 100;
        }
        else{
            p_energy = p_energy + i;
        }
        
     }
     public boolean asPassedOut(){
        if (p_energy == 0){
            return true;
        }
        return false;
    }

     //fonction socialise. LA réaction change en fonciton du niveau de fatigue, diminue energy d'entre 5 et 15, puis donne l'état de la personne.
     public void socialize(){
        int i = (int)(Math.random() * (20 - 5)+5);
        System.out.println("You're talking to " +p_prenom);
        if (p_energy>=50)
            System.out.println(p_prenom+" : WESH ALORS *DABDAB*");
        else if (p_energy >=20)
            System.out.println(p_prenom + " : Oh. i'ts you. Hello !");
        else 
            System.out.println("*"+p_prenom + " " + p_nom + " se roule en boule dans un coins en gémissant faiblement.");
        
        p_energy = p_energy - i;
        if (p_energy<0){
            p_energy =0;
        }
        
        if (p_energy> 60)
            System.out.println(p_prenom + " is feeling fine. "+ p_energy +'\n');
        else if (p_energy > 40)
            System.out.println(p_prenom + " is feeling okay. " + p_energy + '\n');
        else if (p_energy >30)
            System.out.println( p_prenom + "seems tired. " + p_energy+ '\n');
        else if (this.asPassedOut()){
            System.out.println(p_prenom + "collapse onto the ground...");
        }
        else
            System.out.println(p_prenom + " trembles weakly... " + p_energy+ '\n');

        
        
     }

  
    public void day(){

        int actions =8;

       // Scanner sc = new Scanner(System.in);
        System.out.println("Choisissez combient d'actions devra faire Basil aujourd'hui !");
        //actions = sc.nextInt();

        while(actions > 0 && !this.asPassedOut()){
            this.socialize();
            actions--;
        }
     //   sc.close();

    }
    public String toString(){
        return " nom : "+p_nom+ '\t' + " prenom : " + p_prenom + '\t' + " age : " +p_age;
    }
}